mergeInto(LibraryManager.library, {
  
  Hello: function () {
    localStorage.setItem('monChat', 'Tom');
	window.alert('Test alert');
  },

  HelloReturn: function () {
	var cat = localStorage.getItem('monChat');
	var returnStr = cat;
    var bufferSize = lengthBytesUTF8(returnStr) + 1;
    var buffer = _malloc(bufferSize);
    stringToUTF8(returnStr, buffer, bufferSize);
    return buffer;
  },

   ReturnMessage: function (message) {
    var cat = 'TOM';
	var returnStr = localStorage.getItem('monChat');
    var bufferSize = lengthBytesUTF8(returnStr) + 1;
    var buffer = _malloc(bufferSize);
    stringToUTF8(returnStr, buffer, bufferSize);
    return buffer;
  },
  
  ShowMessage: function (message) {
	var cat = localStorage.getItem('monChat');
	console.log(Pointer_stringify(message));
	localStorage.setItem('xml', Pointer_stringify(message));
	var returnStr = localStorage.getItem('xml');
    var bufferSize = lengthBytesUTF8(returnStr) + 1;
    var buffer = _malloc(bufferSize);
    stringToUTF8(returnStr, buffer, bufferSize);
    return buffer;
  },

  SaveDataSession: function (data) {
    localStorage.setItem('xml', Pointer_stringify(data));
  },
  
  GetDataSession: function () {
    var returnStr = localStorage.getItem('xml');
    var bufferSize = lengthBytesUTF8(returnStr) + 1;
    var buffer = _malloc(bufferSize);
    stringToUTF8(returnStr, buffer, bufferSize);
    return buffer;
  },
  
  SendMailData: function () {
    window.open('mailto:d.rabellino@viasphere.fr?subject=TestWebJS&body=blabla');
  },
  
  SendMailDataTest: function (data) {
    window.open('mailto:d.rabellino@viasphere.fr?subject=TestWebJS&body='+Pointer_stringify(data));
  },
  
  PrintFloatArray: function (array, size) {
    for(var i = 0; i < size; i++)
    console.log(HEAPF32[(array >> 2) + i]);
  },

  AddNumbers: function (x, y) {
    return x + y;
  },

  StringReturnValueFunction: function () {
    var returnStr = "bla";
    var bufferSize = lengthBytesUTF8(returnStr) + 1;
    var buffer = _malloc(bufferSize);
    stringToUTF8(returnStr, buffer, bufferSize);
    return buffer;
  },

});