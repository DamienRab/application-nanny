﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagePanel : MonoBehaviour
{
    public GameObject panel;

    //Active l'objet
    public void activePanel()
    {
        panel.SetActive(true);
    }

    //Désactive l'objet
    public void maskPanel()
    {
        panel.SetActive(false);
    }
}
