﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class TrackvelQuizGame : MonoBehaviour
{
    public GameObject popupFin;
    //private Resultat resultats;

    //Les texts
    public Text Question;
    public Text Rep1;
    public Text Rep2;
    public Text Rep3;
    public Text Rep4;
    public Text Rep11;
    public Text Rep21;
    public Text Rep31;
    public Text Rep41;
    public Text Rep12;
    public Text Rep22;
    public Text Rep32;
    public Text Rep42;
    public Text numQuestText;
    public Text Score;

    //Thème
    public Image imgTheme;
    public Sprite imgHistoire;
    public Sprite imgMytho;
    public Sprite imgContemp;
    public Sprite imgPers;


    //Taille Groupe Réponse
    public GameObject ReponsesT0;
    public GameObject ReponsesT1;
    public GameObject ReponsesT2;

    //Variables du script
    public int countRep = 0;
    public int countQuest = 0;
    public int points = 0;
    private Questions questions;
    private List<int> listNumQuest;
    public string fileResultName;
    public string fileQuestions;

    //Class
    //public Timer timer;
    public BtnShow btnShow;
    private ReponseFamily repFamily;
    public ReponseFamily repFamilyT0;
    public ReponseFamily repFamilyT1;
    public ReponseFamily repFamilyT2;

    // Start is called before the first frame update
    void Start()
    {
        /*Gang gang = Gang.LoadFromFile(Application.dataPath + "/Ressources/XML/testXML.xml");
        // Debugger la variable
        Debug.Log(gang.Equipes[0].Personnages[0].Name);*/

        //Ici on tire les questions de la session
        numQuestText.text = "1";
        listNumQuest = new List<int>();
        while (listNumQuest.Count < 10)
        {
            bool numok = true;
            int numRnd = Random.Range(0, 20);
            Debug.Log(numRnd);
            foreach (int i in listNumQuest)
            {
                Debug.Log("val i " + i);
                if (numRnd == i)
                {
                    numok = false;
                }
            }
            if (numok)
            {
                listNumQuest.Add(numRnd);
            }
        }
        //Debug.Log(listNumQuest[0] + " " + listNumQuest[1] + " " + listNumQuest[2] + " " + listNumQuest[3] + " " + listNumQuest[4]);

        /*TextAsset textAsset = (TextAsset) Resources.Load("XML/Resultats");
        TextAsset textAssetQuest = (TextAsset)Resources.Load("XML/FamilyCodeQuestions");*/
        //fileResultName = "jar:file://" + Application.dataPath + "!/assets/Ressources/XML/Resultats.xml";
        //fileQuestions = "jar:file://" + Application.dataPath + "!/assets/Ressources/XML/FamilyCodeQuestions.xml";
        Debug.Log(Application.streamingAssetsPath);
        //Question.text = Application.streamingAssetsPath + "/XML/FamilyCodeQuestions.xml";
        //Question.text = Path.Combine(Application.streamingAssetsPath, "XML/Resultats.xml");
        //resultats = Resultat.LoadFromFile(Application.dataPath + "Resultats.xml");
        //resultats = Resultat.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "XML", "Resultats.xml"));
        //resultats = Resultat.LoadFromFile(Application.streamingAssetsPath + "/XML/FamilyCodeQuestions.xml");
        //Chargement des questions xml
        //questions = Questions.LoadFromFile(Application.dataPath + "FamilyCodeQuestions.xml");
        /*WWW data = new WWW("https://dams-o.github.io/Trackvel/StreamingAssets/XML/TrackvelQuestions.xml");
        yield return data;*/
        StartCoroutine("GetFile");
        //questions = Questions.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "XML", "TrackvelQuestions.xml"));
        //questions = Questions.LoadFromFile(Application.streamingAssetsPath + "/XML/FamilyCodeQuestions.xml");
        // Debugger la variable
        //Debug.Log(questions.arr_Questions[listNumQuest[0]].Reponses[0].description);

        //Première question de la liste
        //chargementQuest(listNumQuest[countQuest]);

    }
    IEnumerator GetFile()
    {
        WWW data = new WWW("https://dams-o.github.io/Trackvel/StreamingAssets/XML/TrackvelQuestions.xml");
        yield return data;
        questions = XmlOperation.DeSerialize(data);
        Debug.Log(listNumQuest[countQuest]);
        chargementQuest(listNumQuest[countQuest]);
    }

    private void chargementQuest(int numQuest)
    {
        //La question
        Question.text = questions.arr_Questions[numQuest].description;

        //Thème
        switch (questions.arr_Questions[numQuest].theme)
        {
            case "Histoire":
                imgTheme.sprite = imgHistoire;
                break;
            case "Mythologie":
                imgTheme.sprite = imgMytho;
                break;
            case "Contemporain":
                imgTheme.sprite = imgContemp;
                break;
            case "Personnage":
                imgTheme.sprite = imgPers;
                break;
            default:
                Debug.Log("ERREUR");
                break;
        }

        //Les réponses
        switch (questions.arr_Questions[numQuest].lg)
        {
            case "0":
                btnShow.changeObjInter(ReponsesT0);
                repFamily = repFamilyT0;
                Rep1.text = questions.arr_Questions[numQuest].Reponses[0].description;
                Rep2.text = questions.arr_Questions[numQuest].Reponses[1].description;
                Rep3.text = questions.arr_Questions[numQuest].Reponses[2].description;
                Rep4.text = questions.arr_Questions[numQuest].Reponses[3].description;
                break;
            case "1":
                btnShow.changeObjInter(ReponsesT1);
                repFamily = repFamilyT1;
                Rep11.text = questions.arr_Questions[numQuest].Reponses[0].description;
                Rep21.text = questions.arr_Questions[numQuest].Reponses[1].description;
                Rep31.text = questions.arr_Questions[numQuest].Reponses[2].description;
                Rep41.text = questions.arr_Questions[numQuest].Reponses[3].description;
                break;
            case "2":
                btnShow.changeObjInter(ReponsesT2);
                repFamily = repFamilyT2;
                Rep12.text = questions.arr_Questions[numQuest].Reponses[0].description;
                Rep22.text = questions.arr_Questions[numQuest].Reponses[1].description;
                Rep32.text = questions.arr_Questions[numQuest].Reponses[2].description;
                Rep42.text = questions.arr_Questions[numQuest].Reponses[3].description;
                break;
            default:
                Debug.Log("ERREUR");
                break;
        }

        countQuest++;
        numQuestText.text = countQuest + "";
    }

    /*public void updateTimer(int time)
    {
        timer.temps = time;
        //timer.reinitTimer();
    }*/

    public void nextQuest()
    {
        if (repFamily.selectedToggle() != 4)
        {
            points = points + questions.arr_Questions[listNumQuest[countQuest - 1]].Reponses[repFamily.selectedToggle()].point;
        }
        if (countQuest < 10)
        {
            chargementQuest(listNumQuest[countQuest]);
        }
        else
        {
            popupFin.SetActive(true);
            Score.text = points.ToString();
            /*resultats.family = points.ToString();
            resultats.familyPlay = "1";
            resultats.Save(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");*/
            Debug.Log(points);
            Debug.Log("Fin du quizzzzzzzz");
        }
    }

}
