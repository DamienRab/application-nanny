﻿using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

public class Equipe
{
    [XmlElement("Personnage")]
    public List<Personnage> Personnages;
    [XmlAttribute("Id")] public int Id;

    private Equipe() { }
}