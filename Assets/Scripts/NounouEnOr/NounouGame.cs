﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class NounouGame : MonoBehaviour
{
    public GameObject popupFin;
    public int points = 0;
    private Resultat resultats;

    //Les texts
    public Text Question;
    public Text Rep1;
    public Text Point1;
    public Text Rep2;
    public Text Point2;
    public Text Rep3;
    public Text Point3;
    public Text Rep4;
    public Text Point4;
    public Text Rep5;
    public Text Point5;

    //Les Toggles
    public ReponseNounou tog1;
    public ReponseNounou tog2;
    public ReponseNounou tog3;
    public ReponseNounou tog4;
    public ReponseNounou tog5;

    //Variables du script
    public int countRep = 0;
    public int countQuest = 0;
    public int pointManche = 0;
    private Questions questions;
    private List<int> listNumQuest;

    [DllImport("__Internal")]
    private static extern void SaveDataSession(string data);

    [DllImport("__Internal")]
    private static extern string GetDataSession();

    // Start is called before the first frame update
    void Start()
    {
        /*Gang gang = Gang.LoadFromFile(Application.dataPath + "/Ressources/XML/testXML.xml");
        // Debugger la variable
        Debug.Log(gang.Equipes[0].Personnages[0].Name);*/

        //Ici on tire les questions de la session
        listNumQuest = new List<int>();
        while(listNumQuest.Count < 5)
        {
            bool numok = true;
            int numRnd = Random.Range(0, 13);
            Debug.Log(numRnd);
            foreach (int i in listNumQuest)
            {
                Debug.Log("val i " +i);
                if (numRnd == i)
                {
                    numok = false;
                }
            }
            if (numok) {
                listNumQuest.Add(numRnd);
            }
        }
        Debug.Log(listNumQuest[0] + " " + listNumQuest[1] + " " + listNumQuest[2] + " " + listNumQuest[3] + " " + listNumQuest[4]);

        //Resultats
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        { //WebGLPlayer
            if (Application.platform == RuntimePlatform.Android)
            {
                resultats = Resultat.LoadFromFile(Application.persistentDataPath + "/Resultats.xml");
            }
            else
            {
                resultats = XmlOperation.DeSerializeResultatsWebStorage(GetDataSession());
            }
        }
        else
        {
            resultats = Resultat.LoadFromFile(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
        }

        //Chargement des questions xml
        //Questions
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        {
            StartCoroutine("GetQuestions");
        }
        else
        {
            questions = Questions.LoadFromFile(Application.dataPath + "/StreamingAssets/XML/NounouQuestions.xml");
            //Première question de la liste
            chargementQuest(listNumQuest[countQuest]);
        }

        // Debugger la variable
        //Debug.Log(questions.arr_Questions[listNumQuest[0]].Reponses[0].description);


    }

    IEnumerator GetQuestions()
    {
        WWW data = new WWW("https://dams-o.github.io/Trackvel/StreamingAssets/XML/NounouQuestions.xml");
        yield return data;

        if (string.IsNullOrEmpty(data.error))
        {
            questions = XmlOperation.DeSerialize(data);
            //Première question de la liste
            chargementQuest(listNumQuest[countQuest]);
        }
    }

    private void chargementQuest(int numQuest)
    {
        //La question
        Question.text = questions.arr_Questions[numQuest].description;

        //Les réponses et points
        Rep1.text = questions.arr_Questions[numQuest].Reponses[0].description;
        Point1.text = questions.arr_Questions[numQuest].Reponses[0].point.ToString();
        Rep2.text = questions.arr_Questions[numQuest].Reponses[1].description;
        Point2.text = questions.arr_Questions[numQuest].Reponses[1].point.ToString();
        Rep3.text = questions.arr_Questions[numQuest].Reponses[2].description;
        Point3.text = questions.arr_Questions[numQuest].Reponses[2].point.ToString();
        Rep4.text = questions.arr_Questions[numQuest].Reponses[3].description;
        Point4.text = questions.arr_Questions[numQuest].Reponses[3].point.ToString();
        Rep5.text = questions.arr_Questions[numQuest].Reponses[4].description;
        Point5.text = questions.arr_Questions[numQuest].Reponses[4].point.ToString();

        countQuest++;
    }

    // Update is called once per frame
    public void nextQuest()
    {
        if(countRep == 3)
        {
            //Les toggles
            pointManche = 0;
            if (tog1.reponseBtnIsCheck)
            {
                pointManche = pointManche + questions.arr_Questions[listNumQuest[countQuest-1]].Reponses[0].point;
            }
            if (tog2.reponseBtnIsCheck)
            {
                pointManche = pointManche + questions.arr_Questions[listNumQuest[countQuest - 1]].Reponses[1].point;
            }
            if (tog3.reponseBtnIsCheck)
            {
                pointManche = pointManche + questions.arr_Questions[listNumQuest[countQuest - 1]].Reponses[2].point;
            }
            if (tog4.reponseBtnIsCheck)
            {
                pointManche = pointManche + questions.arr_Questions[listNumQuest[countQuest - 1]].Reponses[3].point;
            }
            if (tog5.reponseBtnIsCheck)
            {
                pointManche = pointManche + questions.arr_Questions[listNumQuest[countQuest - 1]].Reponses[4].point;
            }
            if (countQuest < 5)
            {
                points = points + pointManche;
                Debug.Log(points +" nouvelle question");
                chargementQuest(listNumQuest[countQuest]);
                tog1.resetToggle();
                tog2.resetToggle();
                tog3.resetToggle();
                tog4.resetToggle();
                tog5.resetToggle();
                countRep = 0;
            }
            else
            {
                points = points + pointManche;
                popupFin.SetActive(true);
                resultats.nounou = (points/2).ToString();
                resultats.nounouPlay = "1";
                resultats.noteGlobal = resultats.noteGlobal + (points / 2);
                resultats.noteFinal = resultats.noteGlobal / 4;

                //La sauvegarde dans le fichier correspondant
                if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
                {
                    if (Application.platform == RuntimePlatform.Android)
                    {
                        resultats.Save(Application.persistentDataPath + "/Resultats.xml");
                    }
                    else
                    {
                        string xmlconvert = XmlOperation.SerializeObject(resultats);
                        SaveDataSession(xmlconvert);
                        Debug.Log(xmlconvert);
                        //resultats.Save("https://dams-o.github.io/Trackvel/StreamingAssets/XML/Resultats.xml");
                    }
                }
                else
                {
                    resultats.Save(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
                }

                Debug.Log("Fin du quizzzzzzzz points : " + points);
            }
        }
    }
}