﻿using UnityEngine;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;

[XmlRoot("Nounou")]
public class Questions
{
    [XmlArray("Questions"), XmlArrayItem("Question")]
    public List<Question> arr_Questions;

    private Questions() { }

    public static Questions LoadFromFile(string filepath)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Questions));
        using (FileStream stream = new FileStream(filepath, FileMode.Open))
        {
            //Debug.Log(serializer.Deserialize(stream).ToString());
            return serializer.Deserialize(stream) as Questions;
        }
    }
}