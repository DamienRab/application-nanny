﻿using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;

public class Question
{
    [XmlAttribute("id")]
    public string Id;
    [XmlElement("Reponse")]
    public List<Reponse> Reponses;
    public string description;
    public string lg;
    public string theme;
}
