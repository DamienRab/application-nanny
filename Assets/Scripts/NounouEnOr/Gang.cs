﻿using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

[XmlRoot("Root")]
public class Gang
{
    [XmlArray("Gang"), XmlArrayItem("Equipe")]
    public List<Equipe> Equipes;

    private Gang() { }

    public static Gang LoadFromFile(string filepath)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Gang));
        using (FileStream stream = new FileStream(filepath, FileMode.Open))
        {
            return serializer.Deserialize(stream) as Gang;
        }
    }
}