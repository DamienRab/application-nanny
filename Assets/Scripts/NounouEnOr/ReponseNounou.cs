﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReponseNounou : MonoBehaviour
{
    //public GameObject points;
    //public Text pointRep;
    //public Toggle reponseToggle;
    public Button reponseBtn;
    public Image reponseBtnImage;
    public bool reponseBtnIsCheck = false;
    public Sprite spriteBlanc;
    public Sprite spriteCheck;
    public NounouGame nounouManager;
    Color whtBtn = Color.white;

    public void showPoint()
    {
        if(nounouManager.countRep < 3 && !reponseBtnIsCheck) {
            //point.text = regleDuJeu;
            //nounouManager.points = nounouManager.points + int.Parse(pointRep.text);
            //reponseToggle.interactable = false;
            //points.SetActive(true);
            reponseBtnIsCheck = true;
            reponseBtnImage.sprite = spriteCheck;
            Debug.Log("check");
            nounouManager.countRep++;
        }
        else if (reponseBtnIsCheck)
        {
            //point.text = regleDuJeu;
            //nounouManager.points = nounouManager.points - int.Parse(pointRep.text);
            //reponseToggle.interactable = false;
            //points.SetActive(true);
            reponseBtnIsCheck = false;
            reponseBtnImage.sprite = spriteBlanc;
            Debug.Log("plus check");
            nounouManager.countRep--;
        }
        else
        {
            /*reponseToggle.interactable = false;
            reponseToggle.isOn = false;
            var colorDisa = reponseToggle.colors;
            var colorSelec = reponseToggle.colors;
            colorDisa.disabledColor = whtBtn;
            colorDisa.selectedColor = whtBtn;*/
            Debug.Log("Trop de réponses");
        }
    }

    /*void Update()
    {
        if (nounouManager.countRep == 3)
        {
            reponseToggle.interactable = false;
        }
    }*/

    public void resetToggle()
    {
        //reponseToggle.interactable = true;
        //points.SetActive(false);
        reponseBtnIsCheck = false;
        reponseBtnImage.sprite = spriteBlanc;
    }
}
