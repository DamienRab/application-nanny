﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnShow : MonoBehaviour
{
    public GameObject btnShow;
    public GameObject objInterac;
    public bool activeInterac = false;

    public void show()
    {
        objInterac.SetActive(true);
        btnShow.SetActive(false);
        activeInterac = true;
    }

    public void dismiss()
    {
        objInterac.SetActive(false);
        btnShow.SetActive(true);
        activeInterac = false;
    }

    public void changeObjInter(GameObject changeObj)
    {
        objInterac = changeObj;
    }
}
