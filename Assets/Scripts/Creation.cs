﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class Creation : MonoBehaviour
{
    private Resultat resultats;
    public InputField prenomCandidatText;
    public Dropdown trancheAge;

    [DllImport("__Internal")]
    private static extern void SaveDataSession(string data);

    [DllImport("__Internal")]
    private static extern string GetDataSession();

    // Start is called before the first frame update
    void Start()
    {
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        { //WebGLPlayer
            if (Application.platform == RuntimePlatform.Android)
            {
                resultats = Resultat.LoadFromFile(Application.persistentDataPath + "/Resultats.xml");
            }
            else //WebGLPlayer
            {
                resultats = XmlOperation.DeSerializeResultatsWebStorage(GetDataSession());
            }
        }
        else
        {
            resultats = Resultat.LoadFromFile(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
        }
        resultats.nounou = "0";
        resultats.nounouPlay = "0";
        resultats.family = "0";
        resultats.familyPlay = "0";
        resultats.jdr = "0";
        resultats.jdrPlay = "0";
        resultats.quizPlay = "0";
        resultats.memory = "0";
        resultats.memoryPlay = "0";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Boutton de connexion
    public void CreationBtn(){
        resultats.prenom = prenomCandidatText.text;
        resultats.trancheAge = trancheAge.captionText.text;
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                resultats.Save(Application.persistentDataPath + "/Resultats.xml");
            }
            else
            {
                string xmlconvert = XmlOperation.SerializeObject(resultats);
                SaveDataSession(xmlconvert);
                Debug.Log(xmlconvert);
            }
        }
        else
        {
            resultats.Save(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
        }
        SceneManager.LoadScene("MenuJeux",  LoadSceneMode.Single);
    }
}
