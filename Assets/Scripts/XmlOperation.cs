﻿using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public static class XmlOperation
{
    public static void Serialize(object item, string path)
    {
        XmlSerializer serializer = new XmlSerializer(item.GetType());
        StreamWriter writer = new StreamWriter(path);
        serializer.Serialize(writer.BaseStream, item);
        writer.Close();
    }
    /*public static void DeserializeSave<Questions>()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Questions));
        StreamReader reader = new StreamReader("https://dams-o.github.io/Trackvel/StreamingAssets/XML/TrackvelQuestions.xml");
        Questions deserialized = (Questions)serializer.Deserialize(reader.BaseStream);
        reader.Close();
        //return deserialized;
    }*/
    public static string SerializeObject<T>(this T toSerialize)
    {
        XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

        using (StringWriter textWriter = new StringWriter())
        {
            xmlSerializer.Serialize(textWriter, toSerialize);
            return textWriter.ToString();
        }
    }
    public static Questions DeSerialize(WWW www)
    {
        using (TextReader textReader = new StringReader(www.text))
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Questions));

            Questions XmlData = serializer.Deserialize(textReader) as Questions;

            return XmlData;
        }
    }

    public static Resultat DeSerializeResultats(WWW www)
    {
        using (TextReader textReader = new StringReader(www.text))
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Resultat));

            Resultat XmlData = serializer.Deserialize(textReader) as Resultat;

            return XmlData;
        }
    }

    public static Resultat DeSerializeResultatsWebStorage(string data)
    {
        using (TextReader textReader = new StringReader(data))
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Resultat));

            Resultat XmlData = serializer.Deserialize(textReader) as Resultat;

            return XmlData;
        }
    }
}
