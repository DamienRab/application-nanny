﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class MenuResultat : MonoBehaviour
{
    private Resultat resultats;
    public Text scoreFamily;
    public Text scoreNounou;
    public Text scoreJDR;
    public Text scoreMemory;
    public Text scoreFinal;
    public GameObject scoreFinalObj;

    public Button btnFamily;
    public Button btnNounou;
    public Button btnJDR;
    public Button btnQuiz;
    public Button btnMemory;

    public Button btnDetailsFamily;
    public Button btnDetailsNounou;
    public Button btnDetailsJDR;
    public Button btnDetailsMemory;

    [DllImport("__Internal")]
    private static extern string GetDataSession();

    // Start is called before the first frame update
    void Start()
    {
        //Chargement des resultat xml
        //Resultats
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                resultats = Resultat.LoadFromFile(Application.persistentDataPath + "/Resultats.xml");
            }
            else
            {
                resultats = XmlOperation.DeSerializeResultatsWebStorage(GetDataSession());
            }
        }
        else
        {
            resultats = Resultat.LoadFromFile(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
        }

        // Debugger la variable
        Debug.Log("résultats : " + resultats.nounou + " " + resultats.family);

        if (resultats.familyPlay == "1")
        {
            btnFamily.interactable = false;
            btnDetailsFamily.interactable = true;
        }
        if (resultats.nounouPlay == "1")
        {
            btnNounou.interactable = false;
            btnDetailsNounou.interactable = true;
        }
        if (resultats.jdrPlay == "1")
        {
            btnJDR.interactable = false;
            btnDetailsJDR.interactable = true;
        }
        if (resultats.quizPlay == "1")
        {
            btnQuiz.interactable = false;
        }
        if (resultats.memoryPlay == "1")
        {
            btnMemory.interactable = false;
            btnDetailsMemory.interactable = true;
        }
        if (resultats.familyPlay == "1" && resultats.nounouPlay == "1" && resultats.jdrPlay == "1" && resultats.quizPlay == "1" && resultats.memoryPlay == "1")
        {
            scoreFinalObj.SetActive(true);
            scoreFinal.text = resultats.noteFinal.ToString();
        }

        scoreFamily.text = resultats.family;
        scoreNounou.text = resultats.nounou;
        scoreJDR.text = resultats.jdr;
        scoreMemory.text = resultats.memory;
    }
}

