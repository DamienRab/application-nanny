﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class QuizGame : MonoBehaviour
{
    public XmlDocument fichierQuestions;
    public GameObject popupFin;
    private Resultat resultats;

    //Les texts
    public Text Question;
    public Text numQuestText;
    public Text theme;

    //Variables du script
    public int countRep = 0;
    public int countQuest = 0;
    public int points = 0;
    private Questions questions;
    private List<int> listNumQuest;
    public string fileResultName;
    public string fileQuestions;

    [DllImport("__Internal")]
    private static extern void SaveDataSession(string data);

    [DllImport("__Internal")]
    private static extern string GetDataSession();

    //Class
    private ReponseFamily repFamily;

    // Start is called before the first frame update
    void Start()
    {
        /*Gang gang = Gang.LoadFromFile(Application.dataPath + "/Ressources/XML/testXML.xml");
        // Debugger la variable
        Debug.Log(gang.Equipes[0].Personnages[0].Name);*/

        //Ici on tire les questions de la session
        numQuestText.text = "1";
        listNumQuest = new List<int>();
        while (listNumQuest.Count < 1)
        {
            bool numok = true;
            int numRnd = Random.Range(0, 4);
            Debug.Log(numRnd);
            foreach (int i in listNumQuest)
            {
                Debug.Log("val i " + i);
                if (numRnd == i)
                {
                    numok = false;
                }
            }
            if (numok)
            {
                listNumQuest.Add(numRnd);
            }
        }
        //Debug.Log(listNumQuest[0] + " " + listNumQuest[1] + " " + listNumQuest[2] + " " + listNumQuest[3] + " " + listNumQuest[4]);

        //Question.text = Path.Combine(Application.streamingAssetsPath, "XML/Resultats.xml");

        //Resultats
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        { //WebGLPlayer
            if (Application.platform == RuntimePlatform.Android)
            {
                resultats = Resultat.LoadFromFile(Application.persistentDataPath + "/Resultats.xml");
            }
            else
            {
                resultats = XmlOperation.DeSerializeResultatsWebStorage(GetDataSession());
            }
        }
        else
        {
            resultats = Resultat.LoadFromFile(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
        }

        //Questions
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        {
            StartCoroutine("GetQuestions");
        }
        else
        {
            questions = Questions.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "XML", "QuizQuestions.xml"));
            //Première question de la liste
            chargementQuest(listNumQuest[countQuest]);
        }
    }

    IEnumerator GetQuestions()
    {
        WWW data = new WWW("https://dams-o.github.io/Trackvel/StreamingAssets/XML/QuizQuestions.xml");
        yield return data;

        if (string.IsNullOrEmpty(data.error))
        {
            questions = XmlOperation.DeSerialize(data);
            //Première question de la liste
            chargementQuest(listNumQuest[countQuest]);
        }
    }

    private void chargementQuest(int numQuest)
    {
        //La question
        Question.text = questions.arr_Questions[numQuest].description;
        theme.text = questions.arr_Questions[numQuest].theme;

        countQuest++;
        numQuestText.text = countQuest +"";
    }

    public void nextQuest()
    {
        popupFin.SetActive(true);
        resultats.quizPlay = "1";
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                resultats.Save(Application.persistentDataPath + "/Resultats.xml");
            }
            else
            {
                string xmlconvert = XmlOperation.SerializeObject(resultats);
                SaveDataSession(xmlconvert);
                Debug.Log(xmlconvert);
            }
        }
        else
        {
            resultats.Save(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
        }
        Debug.Log(points);
        Debug.Log("Fin du quizzzzzzzz");
    }
}