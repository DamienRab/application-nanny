﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("Quiz")]
public class QuestionsQuiz : MonoBehaviour
{
    [XmlArray("Questions"), XmlArrayItem("Question")]
    public List<QuestionQuiz> arr_Questions;

    private QuestionsQuiz() { }

    public static QuestionsQuiz LoadFromFile(string filepath)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(QuestionsQuiz));
        using (FileStream stream = new FileStream(filepath, FileMode.Open))
        {
            //Debug.Log(serializer.Deserialize(stream).ToString());
            return serializer.Deserialize(stream) as QuestionsQuiz;
        }
    }
}
