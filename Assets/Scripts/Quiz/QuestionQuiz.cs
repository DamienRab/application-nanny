﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;

public class QuestionQuiz : MonoBehaviour
{
    [XmlAttribute("id")]
    public string Id;
    public string description;
    public string theme;
}
