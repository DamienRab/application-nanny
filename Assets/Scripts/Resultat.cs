﻿using UnityEngine;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;

[XmlRoot("Resultat")]
public class Resultat
{
    [XmlElement("AdresseAgence")]
    public string adresseAgence;
    [XmlElement("Prenom")]
    public string prenom;
    [XmlElement("TrancheAge")]
    public string trancheAge;
    [XmlElement("Nounou")]
    public string nounou;
    [XmlElement("NounouPlay")]
    public string nounouPlay;
    [XmlElement("Family")]
    public string family;
    [XmlElement("FamilyPlay")]
    public string familyPlay;
    [XmlElement("JDR")]
    public string jdr;
    [XmlElement("JDRPlay")]
    public string jdrPlay;
    [XmlElement("QuizPlay")]
    public string quizPlay;
    [XmlElement("Memory")]
    public string memory;
    [XmlElement("MemoryPlay")]
    public string memoryPlay;
    [XmlElement("noteGlobal")]
    public double noteGlobal;
    [XmlElement("noteFinal")]
    public double noteFinal;

    private Resultat() { }

    public static Resultat LoadFromFile(string filepath)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Resultat));
        using (FileStream stream = new FileStream(filepath, FileMode.Open))
        {
            //Debug.Log(serializer.Deserialize(stream).ToString());
            return serializer.Deserialize(stream) as Resultat;
        }
    }

    public void Save(string path)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Resultat));
        using (FileStream stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }
}