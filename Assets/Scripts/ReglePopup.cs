﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReglePopup : MonoBehaviour
{
    public GameObject popup;
    public GameObject popupBack;
    public GameObject personnages;
    public Text regle;
    public Text titre;
    public string regleDuJeu;
    public string titreDuJeu;

    public void showRegle()
    {
        regle.text = regleDuJeu;
        titre.text = titreDuJeu;
        popup.SetActive(true);
        popupBack.SetActive(true);
        personnages.SetActive(false);
    }

    public void dismissRegle()
    {
        popup.SetActive(false);
        popupBack.SetActive(false);
        personnages.SetActive(true);
    }
}
