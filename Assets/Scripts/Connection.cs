﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Text;
using System.IO;
using System.Xml;
using System.Runtime.InteropServices;

public class Connection : MonoBehaviour
{
    private Resultat resultats;
    public InputField id;
    public InputField pass;
    public Text textErr;
    public Text textMail;
    public Text textErr2;
    public GameObject messErr;
    public GameObject messErr2;
    public GameObject panelMdpOublie;
    public GameObject panelMailEnv;
    private string token = "";
    //public Text testDebug;
    private string path;
    private string savepath;

    [DllImport("__Internal")]
    private static extern void Hello();

    [DllImport("__Internal")]
    private static extern string HelloReturn();

    [DllImport("__Internal")]
    private static extern string ReturnMessage(string message);

    [DllImport("__Internal")]
    private static extern string ShowMessage(string message);

    [DllImport("__Internal")]
    private static extern int AddNumbers(int x, int y);

    [DllImport("__Internal")]
    private static extern string StringReturnValueFunction();

    [DllImport("__Internal")]
    private static extern void SaveDataSession(string data);

    // Start is called before the first frame update
    void Start()
    {
        //resultats = Resultat.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "XML", "Resultats.xml"));
        /*TextAsset textAsset = (TextAsset)Resources.Load("XML/Resultats", typeof(TextAsset));
        XmlDocument xmldoc = new XmlDocument();
        xmldoc.LoadXml(textAsset.text);*/
        StartCoroutine("GetResultat");
        //Hello();
    }

    IEnumerator GetResultat()
    {
        //WWW data = new WWW(Application.dataPath + "/" + "Ressources/XML/Resultats.xml");
        /*if (Application.platform == RuntimePlatform.Android)
            path = Application.persistentDataPath + "!/assets/XML/Resultats.xml";
        else
            path = Application.dataPath + "/" + "Ressources/XML/Resultats.xml";*/

        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        {
            path = "https://dams-o.github.io/Trackvel/StreamingAssets/XML/Resultats.xml";
            if (Application.platform == RuntimePlatform.Android)
            {
                savepath = Application.persistentDataPath + "/Resultats.xml";
            }
            else //WebGLPlayer
            {
                Debug.Log("Toto");
                savepath = "http://www.groupe-ose.fr/nanny/StreamingAssets/XML/Resultats.xml";
            }
        }
        else {
            savepath = path = Application.dataPath + "/StreamingAssets/XML/Resultats.xml";
        }

        WWW data = new WWW(path);
        Debug.Log(savepath);
        yield return data;

        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            string test2 = ShowMessage(data.text);
            Debug.Log(test2);
        }
        if (string.IsNullOrEmpty(data.error))
        {
            resultats = XmlOperation.DeSerializeResultats(data);
            if (Application.platform == RuntimePlatform.Android)
            {
                System.IO.File.WriteAllText(savepath, data.text);
            }
        }
    }

    //Boutton de connexion
    public void ConnectionBtn()
    {

        if (id.text == "" || pass.text == "")
        {
            textErr.text = "Veuillez saisir vos identifiants !";
            messErr.SetActive(true);
            //Debug.Log("Saisir mdp");
        }
        else
        {
            messErr.SetActive(false);
            //textErr.text = "Informations incorrectes";
            //messErr.SetActive(true);
            StartCoroutine(Upload());
        }
        /*else
        {
            SceneManager.LoadScene("NewSessionScene", LoadSceneMode.Single);
        }*/
    }

    //Boutton envoyer pour nouveau mdp
    public void BtnNewMdp()
    {
        if (textMail.text != "test")
        {
            if (textMail.text == "")
            {
                textErr2.text = "Veuillez saisir votre adresse mail";
                messErr2.SetActive(true);
            }
            else
            {
                textErr2.text = "Ce compte n'existe pas";
                messErr2.SetActive(true);
            }
        }
        else
        {
            panelMailEnv.SetActive(true);
        }
    }

    //Boutton me connecter retourne au panel de connection
    public void BtnGoConnect()
    {
        panelMailEnv.SetActive(false);
        panelMdpOublie.SetActive(false);
    }

    IEnumerator Upload()
    {
        /*List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        formData.Add(new MultipartFormDataSection("username=" + id.text + "&password=" + pass.text + "&remember=0&grant_type=password&client_id=1&client_secret=xZvnbYebzXgYT6SkVMbXex8MQt5aXZVLjZXNtcRc&scope="));
        */
        Dictionary<string, string> formData = new Dictionary<string, string>();
        formData.Add("username", id.text);
        formData.Add("password", pass.text);
        formData.Add("remember", "0");
        formData.Add("grant_type", "password");
        formData.Add("client_id", "1");
        formData.Add("client_secret", "xZvnbYebzXgYT6SkVMbXex8MQt5aXZVLjZXNtcRc");
        formData.Add("scope", "");

        // il aurait fallut utiliser formData.Insert pour se servir de la nouvelle méthode

        Debug.Log(id.text + " " + pass.text);
        WWWForm form = new WWWForm();
        form.AddField("username", id.text);
        form.AddField("password", pass.text);
        form.AddField("remember", "0");
        form.AddField("grant_type", "password");
        form.AddField("client_id", "1");
        form.AddField("client_secret", "xZvnbYebzXgYT6SkVMbXex8MQt5aXZVLjZXNtcRc");
        form.AddField("scope", "");


        /*UnityWebRequest www = UnityWebRequest.Post("https://einstein.merciplus.fr/api/loginNanny", formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error +" " + www.url);
            textErr.text = "Informations incorrectes";
            messErr.SetActive(true);
        }
        else
        {
            Debug.Log("Post ok " + www.GetRequestHeader("access_token"));
            StringBuilder sb = new StringBuilder();
            foreach (System.Collections.Generic.KeyValuePair<string, string> dict in www.GetResponseHeaders())
            {
                sb.Append(dict.Key).Append(": \t[").Append(dict.Value).Append("]\n");
            }

            // Print Headers
            Debug.Log(sb.ToString());

            // Print Body
            Debug.Log(www.downloadHandler.text.Substring(61));
            token = www.downloadHandler.text.Substring(61);
            Debug.Log(token.Substring(0, token.Length-2));

            resultats.adresseAgence = id.text;
            resultats.Save(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
            SceneManager.LoadScene("NewSessionScene", LoadSceneMode.Single);
        }*/

        WWW www = new WWW("https://einstein.merciplus.fr/api/loginNanny", form);
        yield return www;

        if (www.error == "")
        {
            Debug.Log(www.error + " " + www.url);
            textErr.text = "Informations incorrectes";
            messErr.SetActive(true);
        }
        else
        {
            if (www.text == "\"unauthorized\"")
            {
                Debug.Log(www.text + " " + www.url);
                textErr.text = "Informations incorrectes";
                messErr.SetActive(true);
            }
            if (www.text.Substring(0,3) == "<!d")
            {
                Debug.Log("Ce ne sont pas des id" + " " + www.url);
                textErr.text = "Informations non conformes";
                messErr.SetActive(true);
            }
            if (!(www.text == "\"unauthorized\"") && !(www.text.Substring(0,3) == "<!d"))
            {
                Debug.Log("Post ok " + www.text);
                StringBuilder sb = new StringBuilder();
                foreach (System.Collections.Generic.KeyValuePair<string, string> dict in www.responseHeaders)
                {
                    sb.Append(dict.Key).Append(": \t[").Append(dict.Value).Append("]\n");
                }
                /*string testjsstorage = HelloReturn();
                int result = AddNumbers(5, 7);
                string testmess2 = StringReturnValueFunction();
                Debug.Log(result);
                Debug.Log(testmess);
                Debug.Log(testmess2);
                Debug.Log(testjsstorage);*/
                // Print Headers
                Debug.Log(sb.ToString());

                // Print Body
                /*Debug.Log(www.text.Substring(61));
                token = www.text.Substring(61);
                Debug.Log(token.Substring(0, token.Length - 2));*/

                resultats.adresseAgence = id.text;
                //XmlOperation.Serialize(resultats, Application.dataPath + "/" + "Ressources/XML/Resultats.xml");
                //XmlOperation.Serialize(resultats, savepath);
                //resultats.Save("http://www.groupe-ose.fr/nanny/StreamingAssets/XML/Resultats.xml");

                SaveData();

                //ShowMessage("Toto c'est envoyé par javascript");
                SceneManager.LoadScene("NewSessionScene", LoadSceneMode.Single);
            }
        }
    }
    //Boutton me connecter retourne au panel de connection
    public void SaveData()
    {
        Debug.Log("Saving");
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            //StartCoroutine(UploadData());

            //Sauvegarde via loclstorage JS
            string xmlconvert = XmlOperation.SerializeObject(resultats);
            SaveDataSession(xmlconvert);
            Debug.Log(xmlconvert);
        }
        else
        {
            //StartCoroutine(UploadData());
            XmlOperation.Serialize(resultats, savepath);
        }
    }

    IEnumerator UploadData()
    {
        byte[] myData = System.Text.Encoding.UTF8.GetBytes("This is some test data");
        UnityWebRequest www = UnityWebRequest.Put("http://www.groupe-ose.fr/nanny/StreamingAssets/XML/Resultats.xml", myData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Upload complete!");
        }
    }
}
