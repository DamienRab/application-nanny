﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RetourMenuScript : MonoBehaviour
{
    //Boutton retour menu
    public void RetourBtn(){
        SceneManager.LoadScene("MenuJeux",  LoadSceneMode.Single);
    }
}
