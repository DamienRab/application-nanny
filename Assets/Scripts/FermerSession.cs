﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Net;
using System.Net.Mail;
using System.Runtime.InteropServices;
using UE.Email;

public class FermerSession : MonoBehaviour
{
    private Resultat resultats;

    [DllImport("__Internal")]
    private static extern void SaveDataSession(string data);

    [DllImport("__Internal")]
    private static extern string GetDataSession();

    [DllImport("__Internal")]
    private static extern void SendMailData();

    [DllImport("__Internal")]
    private static extern void SendMailDataTest(string data);

    // Start is called before the first frame update
    void Start()
    {
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        { //WebGLPlayer
            if (Application.platform == RuntimePlatform.Android)
            {
                resultats = Resultat.LoadFromFile(Application.persistentDataPath + "/Resultats.xml");
            }
            else
            {
                resultats = XmlOperation.DeSerializeResultatsWebStorage(GetDataSession());
            }
        }
        else
        {
            resultats = Resultat.LoadFromFile(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
        }
    }

    // Update is called once per frame
    public void envoieResultatMail()
    {
        MailMessage mail = new MailMessage();
        SmtpClient SmtpServer = new SmtpClient("in-v3.mailjet.com");
        Debug.Log("testmail");
        mail.From = new MailAddress("appliNanny@family-sphere.com");
        //Check pour adresse de test
        if(resultats.adresseAgence == "franchise@test.fr")
        {
            mail.To.Add("d.rabellino@viasphere.fr");
        }
        else
        {
            mail.To.Add(resultats.adresseAgence);
        }
        mail.Subject = "Résultat Nanny de la session de " + resultats.prenom;
        mail.Body = "Compte rendu de la session Nanny de " + resultats.prenom + "\n" +
            "Tranche d'age sélectionné : " + resultats.trancheAge + "\n" +
            "Résultat sur le jeu Family Code : " + resultats.family + "/25\n" +
            "Résultat sur le Jeu des risques : " + resultats.jdr + "/25\n" +
            "Résultat sur le jeu Nounou en or : " + resultats.nounou + "/25\n" +
            "Résultat sur le Memory : " + resultats.memory + "/25\n" +
            "Résultat global : " + resultats.noteGlobal + "/100\n\n" +
            "Note sur 20 : " + resultats.noteFinal + "/20\n"
            ;

        //System.Net.Mail.Attachment attachment;
        //attachment = new System.Net.Mail.Attachment("c:/textfile.txt");
        //mail.Attachments.Add(attachment);

        SmtpServer.Port = 587;
        //SmtpServer.Port = 25;
        SmtpServer.Credentials = new System.Net.NetworkCredential("27e6583b8c05a97fbbcbc149d4477fd8", "2be0c71d1728756eddc7784f2d0130ba");
        SmtpServer.EnableSsl = true;

        if (Application.platform == RuntimePlatform.WebGLPlayer)
        { //WebGLPlayer
            Debug.Log("Mail Web JS");
            Email.SendEmail("appliNanny@family-sphere.com", "d.rabellino@viasphere.fr", mail.Subject, mail.Body, "in-v3.mailjet.com", "27e6583b8c05a97fbbcbc149d4477fd8", "2be0c71d1728756eddc7784f2d0130ba");
            //SendMailDataTest(mail.Body);
        }
        else
        {
            SmtpServer.Send(mail);
        }
        //SceneManager.LoadScene("NewSessionScene", LoadSceneMode.Single);
    }

    public void fermerSession()
    {
        //resultats.adresseAgence = ""; ON LAISSE LA CONNEXION ACTIVE
        resultats.prenom = "";
        resultats.trancheAge = "";
        resultats.nounou = "0";
        resultats.nounouPlay = "0";
        resultats.family = "0";
        resultats.familyPlay = "0";
        resultats.jdr = "0";
        resultats.jdrPlay = "0";
        resultats.quizPlay = "0";
        resultats.memory = "0";
        resultats.memoryPlay = "0";
        resultats.noteGlobal = 0;
        resultats.noteFinal = 0;
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        { //WebGLPlayer
            if (Application.platform == RuntimePlatform.Android)
            {
                resultats.Save(Application.persistentDataPath + "/Resultats.xml");
            }
            else
            {
                string xmlconvert = XmlOperation.SerializeObject(resultats);
                SaveDataSession(xmlconvert);
                Debug.Log(xmlconvert);
                //resultats.Save("https://dams-o.github.io/Trackvel/StreamingAssets/XML/Resultats.xml");
            }
        }
        else
        {
            resultats.Save(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
        }
        SceneManager.LoadScene("NewSessionScene", LoadSceneMode.Single);
    }
}
