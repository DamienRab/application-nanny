﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    public AudioSource sAudio;

    // Update is called once per frame
    public void playSound()
    {
        sAudio.Play();
    }
}
