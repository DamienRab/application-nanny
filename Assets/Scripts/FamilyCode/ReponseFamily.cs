﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReponseFamily : MonoBehaviour
{
    //Les Toggles
    public Toggle tog1;
    public Toggle tog2;
    public Toggle tog3;
    public Toggle tog4;


    public void toggleChanged(Toggle reponseToggle)
    {
        if (reponseToggle.isOn)
        {
            if (reponseToggle != tog1)
            {
                tog1.isOn = false;
            }
            if (reponseToggle != tog2)
            {
                tog2.isOn = false;
            }
            if (reponseToggle != tog3)
            {
                tog3.isOn = false;
            }
            if (reponseToggle != tog4)
            {
                tog4.isOn = false;
            }
        }
    }

    public int selectedToggle()
    {
        if (tog1.isOn)
        {
            return 0;
        }
        if (tog2.isOn)
        {
            return 1;
        }
        if (tog3.isOn)
        {
            return 2;
        }
        if (tog4.isOn)
        {
            return 3;
        }
        return 4;
    }
}
