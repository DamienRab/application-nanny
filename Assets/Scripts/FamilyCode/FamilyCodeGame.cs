﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine.UI;
using System.IO;
using System.Runtime.InteropServices;

public class FamilyCodeGame : MonoBehaviour
{
    public GameObject popupFin;
    private Resultat resultats;

    //Les texts
    public Text Question;
    public Text Rep1;
    public Text Rep2;
    public Text Rep3;
    public Text Rep4;
    public Text Rep11;
    public Text Rep21;
    public Text Rep31;
    public Text Rep41;
    public Text Rep12;
    public Text Rep22;
    public Text Rep32;
    public Text Rep42;
    public Text numQuestText;

    //Les toggles
    public Toggle Tog1;
    public Toggle Tog2;
    public Toggle Tog3;
    public Toggle Tog4;
    public Toggle Tog11;
    public Toggle Tog21;
    public Toggle Tog31;
    public Toggle Tog41;
    public Toggle Tog12;
    public Toggle Tog22;
    public Toggle Tog32;
    public Toggle Tog42;

    //Thème
    public Image imgTheme;
    public Sprite imgActivite;
    public Sprite imgEveil;
    public Sprite imgHygiene;
    public Sprite imgRepas;
    public Sprite imgSante;
    public Sprite imgSommeil;
    public Sprite imgSortie;
    public Sprite imgTransmission;
    public Sprite imgUrgence;

    //Taille Groupe Réponse
    public GameObject ReponsesT0;
    public GameObject ReponsesT1;
    public GameObject ReponsesT2;

    //Variables du script
    public int countRep = 0;
    public int countQuest = 0;
    public int points= 0;
    private Questions questions;
    private List<int> listNumQuest;
    public string fileResultName;
    public string fileQuestions;

    //Class
    public Timer timer;
    public BtnShow btnShow;
    private ReponseFamily repFamily;
    public ReponseFamily repFamilyT0;
    public ReponseFamily repFamilyT1;
    public ReponseFamily repFamilyT2;

    [DllImport("__Internal")]
    private static extern void SaveDataSession(string data);

    [DllImport("__Internal")]
    private static extern string GetDataSession();

    // Start is called before the first frame update
    void Start()
    {
        /*Gang gang = Gang.LoadFromFile(Application.dataPath + "/Ressources/XML/testXML.xml");
        // Debugger la variable
        Debug.Log(gang.Equipes[0].Personnages[0].Name);*/

        //Ici on tire les questions de la session
        numQuestText.text = "1";
        listNumQuest = new List<int>();
        while(listNumQuest.Count < 10)
        {
            bool numok = true;
            int numRnd = Random.Range(0, 22);
            Debug.Log(numRnd);
            foreach (int i in listNumQuest)
            {
                Debug.Log("val i " +i);
                if (numRnd == i)
                {
                    numok = false;
                }
            }
            if (numok) {
                listNumQuest.Add(numRnd);
            }
        }
        Debug.Log(listNumQuest[0] + " " + listNumQuest[1] + " " + listNumQuest[2] + " " + listNumQuest[3] + " " + listNumQuest[4]);

        /*TextAsset textAsset = (TextAsset) Resources.Load("XML/Resultats");
        TextAsset textAssetQuest = (TextAsset)Resources.Load("XML/FamilyCodeQuestions");*/
        //fileResultName = "jar:file://" + Application.dataPath + "!/assets/Ressources/XML/Resultats.xml";
        //fileQuestions = "jar:file://" + Application.dataPath + "!/assets/Ressources/XML/FamilyCodeQuestions.xml";
        //Debug.Log(Application.streamingAssetsPath);
        //Question.text = Application.streamingAssetsPath + "/XML/FamilyCodeQuestions.xml";
        //Question.text = Path.Combine(Application.streamingAssetsPath, "XML/Resultats.xml");
        //resultats = Resultat.LoadFromFile(Application.dataPath + "Resultats.xml");

        //Resultats
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        { //WebGLPlayer
            if (Application.platform == RuntimePlatform.Android)
            {
                resultats = Resultat.LoadFromFile(Application.persistentDataPath + "/Resultats.xml");
            }
            else
            {
                resultats = XmlOperation.DeSerializeResultatsWebStorage(GetDataSession());
                //StartCoroutine("GetResultat");
            }
        }
        else
        {
            resultats = Resultat.LoadFromFile(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
        }

        //resultats = Resultat.LoadFromFile(Application.streamingAssetsPath + "/XML/FamilyCodeQuestions.xml");
        //Chargement des questions xml
        //questions = Questions.LoadFromFile(Application.dataPath + "FamilyCodeQuestions.xml");

        //Questions
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        {
            StartCoroutine("GetQuestions");
        }
        else
        {
            questions = Questions.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "XML", "FamilyCodeQuestions.xml"));
            //Première question de la liste
            chargementQuest(listNumQuest[countQuest]);
        }

        //questions = Questions.LoadFromFile(Application.streamingAssetsPath + "/XML/FamilyCodeQuestions.xml");
        // Debugger la variable
        //Debug.Log(questions.arr_Questions[listNumQuest[0]].Reponses[0].description);


    }

    /*IEnumerator GetResultat()
    {
        WWW data = new WWW("https://dams-o.github.io/Trackvel/StreamingAssets/XML/Resultats.xml");
        yield return data;

        if (string.IsNullOrEmpty(data.error))
        {
            resultats = XmlOperation.DeSerializeResultats(data);
        }
    }*/

    IEnumerator GetQuestions()
    {
        WWW data = new WWW("https://dams-o.github.io/Trackvel/StreamingAssets/XML/FamilyCodeQuestions.xml");
        yield return data;

        if (string.IsNullOrEmpty(data.error))
        {
            questions = XmlOperation.DeSerialize(data);
            //Première question de la liste
            chargementQuest(listNumQuest[countQuest]);
        }
    }

    private void chargementQuest(int numQuest)
    {
        //La question
        Question.text = questions.arr_Questions[numQuest].description;

        //Thème
        switch (questions.arr_Questions[numQuest].theme)
        {
            case "Activite":
                imgTheme.sprite = imgActivite;
                break;
            case "Eveil":
                imgTheme.sprite = imgEveil;
                break;
            case "Hygiene":
                imgTheme.sprite = imgHygiene;
                break;
            case "Repas":
                imgTheme.sprite = imgRepas;
                break;
            case "Sante":
                imgTheme.sprite = imgSante;
                break;
            case "Sommeil":
                imgTheme.sprite = imgSommeil;
                break;
            case "Sortie":
                imgTheme.sprite = imgSortie;
                break;
            case "Transmission":
                imgTheme.sprite = imgTransmission;
                break;
            case "Urgence":
                imgTheme.sprite = imgUrgence;
                break;
            default:
                Debug.Log("ERREUR Thème");
                break;
        }

        //Les réponses
        switch (questions.arr_Questions[numQuest].lg)
        {
            case "0":
                btnShow.changeObjInter(ReponsesT0);
                repFamily = repFamilyT0;
                Rep1.text = questions.arr_Questions[numQuest].Reponses[0].description;
                Rep2.text = questions.arr_Questions[numQuest].Reponses[1].description;
                Rep3.text = questions.arr_Questions[numQuest].Reponses[2].description;
                Rep4.text = questions.arr_Questions[numQuest].Reponses[3].description;
                Tog1.isOn = false;
                Tog2.isOn = false;
                Tog3.isOn = false;
                Tog4.isOn = false;
                break;
            case "1":
                btnShow.changeObjInter(ReponsesT1);
                repFamily = repFamilyT1;
                Rep11.text = questions.arr_Questions[numQuest].Reponses[0].description;
                Rep21.text = questions.arr_Questions[numQuest].Reponses[1].description;
                Rep31.text = questions.arr_Questions[numQuest].Reponses[2].description;
                Rep41.text = questions.arr_Questions[numQuest].Reponses[3].description;
                Tog11.isOn = false;
                Tog21.isOn = false;
                Tog31.isOn = false;
                Tog41.isOn = false;
                break;
            case "2":
                btnShow.changeObjInter(ReponsesT2);
                repFamily = repFamilyT2;
                Rep12.text = questions.arr_Questions[numQuest].Reponses[0].description;
                Rep22.text = questions.arr_Questions[numQuest].Reponses[1].description;
                Rep32.text = questions.arr_Questions[numQuest].Reponses[2].description;
                Rep42.text = questions.arr_Questions[numQuest].Reponses[3].description;
                Tog12.isOn = false;
                Tog22.isOn = false;
                Tog32.isOn = false;
                Tog42.isOn = false;
                break;
            default:
                Debug.Log("ERREUR lg");
                break;
        }

        countQuest++;
        numQuestText.text = countQuest +"";
    }

    public void updateTimer(int time)
    {
        timer.temps = time;
        //timer.reinitTimer();
    }

    public void nextQuest()
    {
        if (repFamily.selectedToggle() != 4)
        {
            points = points + questions.arr_Questions[listNumQuest[countQuest -1]].Reponses[repFamily.selectedToggle()].point;
        }
        if (countQuest < 10)
        {
            chargementQuest(listNumQuest[countQuest]);
        }
        else
        {
            popupFin.SetActive(true);
            resultats.family = ((points*5)/6).ToString();
            resultats.familyPlay = "1";
            resultats.noteGlobal = resultats.noteGlobal + ((points * 5) / 6);
            resultats.noteFinal = resultats.noteGlobal / 4;
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
            {
                if (Application.platform == RuntimePlatform.Android)
                {
                    resultats.Save(Application.persistentDataPath + "/Resultats.xml");
                }
                else
                {
                    string xmlconvert = XmlOperation.SerializeObject(resultats);
                    SaveDataSession(xmlconvert);
                    Debug.Log(xmlconvert);
                    //resultats.Save("https://dams-o.github.io/Trackvel/StreamingAssets/XML/Resultats.xml");
                }
            }
            else
            {
                resultats.Save(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
            }
            Debug.Log(points);
            Debug.Log("Fin du quizzzzzzzz");
        }
    }

}