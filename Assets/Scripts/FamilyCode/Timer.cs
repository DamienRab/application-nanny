﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float temps = 10;
    public Text timerText;
    public int tempsNum;

    //Chronos
    /*public GameObject chrono0;
    public GameObject chrono1;
    public GameObject chrono2;
    public GameObject chrono3;
    public GameObject chrono4;
    public GameObject chrono5;
    public GameObject chrono6;
    public GameObject chrono7;
    public GameObject chronoF;*/

    //Manager
    public FamilyCodeGame managerFamily;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tempsNum = Mathf.RoundToInt(temps);
        timerText.text = (tempsNum + "");
        if(temps >= 0)
        {
            temps -= Time.deltaTime;
        }
        else if (temps < 0)
        {
            if (managerFamily.btnShow.activeInterac)
            {
                temps = 10;
                managerFamily.btnShow.dismiss();
                managerFamily.nextQuest();
            }
            else
            {
                temps = 15;
                managerFamily.btnShow.show();
            }
        }
        /*if (temps > 14)
        {

        }
        else if (temps < 14 && temps > 13)
        {
            chrono0.SetActive(false);
            chrono1.SetActive(true);
        }
        else if (temps < 13 && temps > 11)
        {
            chrono1.SetActive(false);
            chrono2.SetActive(true);
        }
        else if (temps < 11 && temps > 9)
        {
            chrono2.SetActive(false);
            chrono3.SetActive(true);
        }
        else if (temps < 9 && temps > 7)
        {
            chrono3.SetActive(false);
            chrono4.SetActive(true);
        }
        else if (temps < 7 && temps > 5)
        {
            chrono4.SetActive(false);
            chrono5.SetActive(true);
        }
        else if (temps < 5 && temps > 3)
        {
            chrono5.SetActive(false);
            chrono6.SetActive(true);
        }
        else if (temps < 3 && temps > 1)
        {
            chrono6.SetActive(false);
            chrono7.SetActive(true);
        }
        else if (temps < 0)
        {
            if (managerFamily.btnShow.activeInterac)
            {
                temps = 10;
                managerFamily.btnShow.dismiss();
                managerFamily.nextQuest();
                reinitTimer();
            }
            else
            {
                temps = 15;
                managerFamily.btnShow.show();
                reinitTimer();
            }
        }
        else
        {
            chrono7.SetActive(false);
            chronoF.SetActive(true);
        }*/
    }

    //Réinitialisation du timer
    /*public void reinitTimer()
    {
        chrono0.SetActive(true);
        chrono1.SetActive(false);
        chrono2.SetActive(false);
        chrono3.SetActive(false);
        chrono4.SetActive(false);
        chrono5.SetActive(false);
        chrono6.SetActive(false);
        chrono7.SetActive(false);
        chronoF.SetActive(false);
    }*/
}
