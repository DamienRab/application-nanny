﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LancerJeu : MonoBehaviour
{
    public Text titreDuJeu;

    //Lancer scene jeu
    public void LancerBtn()
    {
        SceneManager.LoadScene(titreDuJeu.text, LoadSceneMode.Single);
    }
}
