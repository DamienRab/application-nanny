﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.InteropServices;

public class TouchScreenJDR : MonoBehaviour
{
    private Resultat resultats;

    public GameObject markTap1;
    public GameObject markTap2;
    public GameObject markTap3;
    public GameObject markTap4;
    public GameObject markTap5;
    public GameObject markTap6;
    public GameObject markTap7;

    //Les differentes planches
    public GameObject sortieEcole;
    public GameObject salleDeBain;
    public GameObject parc;
    public GameObject chambre;
    public GameObject cuisine;
    public GameObject salon;

    public GameObject popupFin;

    public bool fait = false;
    public int countTap = 0;
    public int countGoodTap = 0;
    public int score = 0;
    private int numRnd;
    private int numRnd2;

    [DllImport("__Internal")]
    private static extern void SaveDataSession(string data);

    [DllImport("__Internal")]
    private static extern string GetDataSession();

    // Start is called before the first frame update
    void Start()
    {
        popupFin = GameObject.Find("PopupFin");
        popupFin.SetActive(false);

        //Resultats
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                resultats = Resultat.LoadFromFile(Application.persistentDataPath + "/Resultats.xml");
            }
            else
            {
                resultats = XmlOperation.DeSerializeResultatsWebStorage(GetDataSession());
            }
        }
        else
        {
            resultats = Resultat.LoadFromFile(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
        }

        numRnd = Random.Range(0, 5);
        numRnd2 = Random.Range(0, 5);
        if (numRnd == numRnd2)
        {
            if(numRnd2 > 2)
            {
                numRnd2 -= 1;
            }
            else
            {
                numRnd2 += 1;
            }
        }
        switch (numRnd)
        {
            case 0:
                sortieEcole.SetActive(true);
                break;
            case 1:
                salleDeBain.SetActive(true);
                break;
            case 2:
                parc.SetActive(true);
                break;
            case 3:
                chambre.SetActive(true);
                break;
            case 4:
                cuisine.SetActive(true);
                break;
            case 5:
                salon.SetActive(true);
                break;
            default:
                Debug.Log("Erreur num");
                break;
        }
    }

    public void touchOnScreen()
    {
        countTap++;
        switch (countTap)
        {
            case 1:
                markTap1.SetActive(true);
                break;
            case 2:
                markTap2.SetActive(true);
                break;
            case 3:
                markTap3.SetActive(true);
                break;
            case 4:
                markTap4.SetActive(true);
                break;
            case 5:
                markTap5.SetActive(true);
                break;
            case 6:
                markTap6.SetActive(true);
                break;
            case 7:
                markTap7.SetActive(true);
                finDeJeu();
                break;
            default:
                Debug.Log("TAP");
                break;
        }
    }

    public void makeGoodTap()
    {
        countGoodTap++;
        if (countGoodTap >= 5)
        {
            finDeJeu();
        }
    }

    private void finDeJeu()
    {
        if (fait)
        {
            popupFin.SetActive(true);
            resultats.jdr = (score+(countGoodTap * 2.5)).ToString();
            resultats.jdrPlay = "1";
            resultats.noteGlobal = resultats.noteGlobal + (score + (countGoodTap * 2.5));
            resultats.noteFinal = resultats.noteGlobal / 4;

            //La sauvegarde dans le fichier correspondant
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
            {
                if (Application.platform == RuntimePlatform.Android)
                {
                    resultats.Save(Application.persistentDataPath + "/Resultats.xml");
                }
                else
                {
                    string xmlconvert = XmlOperation.SerializeObject(resultats);
                    SaveDataSession(xmlconvert);
                    Debug.Log(xmlconvert);
                    //resultats.Save("https://dams-o.github.io/Trackvel/StreamingAssets/XML/Resultats.xml");
                }
            }
            else
            {
                resultats.Save(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
            }
        }
        else
        {
            sortieEcole.SetActive(false);
            salleDeBain.SetActive(false);
            parc.SetActive(false);
            chambre.SetActive(false);
            cuisine.SetActive(false);
            salon.SetActive(false);
            switch (numRnd2)
            {
                case 0:
                    sortieEcole.SetActive(true);
                    break;
                case 1:
                    salleDeBain.SetActive(true);
                    break;
                case 2:
                    parc.SetActive(true);
                    break;
                case 3:
                    chambre.SetActive(true);
                    break;
                case 4:
                    cuisine.SetActive(true);
                    break;
                case 5:
                    salon.SetActive(true);
                    break;
                default:
                    Debug.Log("Erreur num");
                    break;
            }
            salleDeBain.SetActive(true);
            score = (countGoodTap * 3);
            fait = true;
            markTap1.SetActive(false);
            markTap2.SetActive(false);
            markTap3.SetActive(false);
            markTap4.SetActive(false);
            markTap5.SetActive(false);
            markTap6.SetActive(false);
            markTap7.SetActive(false);
            countGoodTap = 0;
            countTap = 0;
        }
    }
}
