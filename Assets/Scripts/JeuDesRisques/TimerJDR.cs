﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerJDR : MonoBehaviour
{
    public float temps = 50;
    public Text timerText;
    public int tempsNum;

    //Manager


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tempsNum = Mathf.RoundToInt(temps);
        timerText.text = (tempsNum + "");
        if(temps >= 0)
        {
            temps -= Time.deltaTime;
        }
        else if (temps < 0)
        {
            if (true)
            {
                temps = 10;
            }
            else
            {
                temps = 15;
            }
        }
    }

    public void stopTimer()
    {

    }

}
