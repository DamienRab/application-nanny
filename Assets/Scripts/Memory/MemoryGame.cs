﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine.UI;
using System.IO;
using System.Runtime.InteropServices;

public class MemoryGame : MonoBehaviour
{
    public GameObject popupFin;
    private Resultat resultats;

    //Les texts
    public Text Question;
    public Text Rep1;
    public Text Rep2;
    public Text Rep3;
    public Text Rep4;
    public Text Rep11;
    public Text Rep21;
    public Text Rep31;
    public Text Rep41;
    public Text numQuestText;

    //Taille Groupe Réponse
    public GameObject ReponsesT0;
    public GameObject ReponsesT1;

    //Variables du script
    public int countRep = 0;
    public int countQuest = 0;
    public int points = 0;
    private Questions questions;
    private List<int> listNumQuest;
    private int numRnd = 0;
    //public string fileResultName;
    //public string fileQuestions;

    //Class
    public BtnShow btnShow;
    private ReponseFamily repFamily;
    public ReponseFamily repFamilyT0;
    public ReponseFamily repFamilyT1;

    [DllImport("__Internal")]
    private static extern void SaveDataSession(string data);

    [DllImport("__Internal")]
    private static extern string GetDataSession();

    // Start is called before the first frame update
    void Start()
    {
        //Ici on tire les questions de la session
        numQuestText.text = "1";
        listNumQuest = new List<int>();
        while (listNumQuest.Count < 5)
        {
            bool numok = true;

            //int numRnd = Random.Range(0, 22);
            Debug.Log(numRnd);
            foreach (int i in listNumQuest)
            {
                Debug.Log("val i " + i);
                if (numRnd == i)
                {
                    numok = false;
                }
            }
            if (numok)
            {
                listNumQuest.Add(numRnd);
                numRnd++;
            }
        }
        Debug.Log(listNumQuest[0] + " " + listNumQuest[1] + " " + listNumQuest[2] + " " + listNumQuest[3] + " " + listNumQuest[4]);

        Debug.Log(Application.streamingAssetsPath);
        //Question.text = Path.Combine(Application.streamingAssetsPath, "XML/Resultats.xml");

        //Resultats
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        { //WebGLPlayer
            if (Application.platform == RuntimePlatform.Android)
            {
                resultats = Resultat.LoadFromFile(Application.persistentDataPath + "/Resultats.xml");
            }
            else
            {
                resultats = XmlOperation.DeSerializeResultatsWebStorage(GetDataSession());
            }
        }
        else
        {
            resultats = Resultat.LoadFromFile(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
        }

        //Questions
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        {
            StartCoroutine("GetQuestions");
        }
        else
        {
            questions = Questions.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "XML", "MemoryQuestions.xml"));
            //Première question de la liste
            chargementQuest(listNumQuest[countQuest]);
        }
    }

    IEnumerator GetQuestions()
    {
        WWW data = new WWW("https://dams-o.github.io/Trackvel/StreamingAssets/XML/MemoryQuestions.xml");
        yield return data;

        if (string.IsNullOrEmpty(data.error))
        {
            questions = XmlOperation.DeSerialize(data);
            //Première question de la liste
            chargementQuest(listNumQuest[countQuest]);
        }
    }

    private void chargementQuest(int numQuest)
    {
        //La question
        Question.text = questions.arr_Questions[numQuest].description;

        //Thème
        /*switch (questions.arr_Questions[numQuest].theme)
        {
            case "Activite":
                imgTheme.sprite = imgActivite;
                break;
            case "Eveil":
                imgTheme.sprite = imgEveil;
                break;
            case "Hygiene":
                imgTheme.sprite = imgHygiene;
                break;
            case "Repas":
                imgTheme.sprite = imgRepas;
                break;
            case "Sante":
                imgTheme.sprite = imgSante;
                break;
            case "Sommeil":
                imgTheme.sprite = imgSommeil;
                break;
            case "Sortie":
                imgTheme.sprite = imgSortie;
                break;
            case "Transmission":
                imgTheme.sprite = imgTransmission;
                break;
            case "Urgence":
                imgTheme.sprite = imgUrgence;
                break;
            default:
                Debug.Log("ERREUR");
                break;
        }*/

        //Les réponses
        switch (questions.arr_Questions[numQuest].lg)
        {
            case "0":
                btnShow.changeObjInter(ReponsesT0);
                repFamily = repFamilyT0;
                Rep1.text = questions.arr_Questions[numQuest].Reponses[0].description;
                Rep2.text = questions.arr_Questions[numQuest].Reponses[1].description;
                Rep3.text = questions.arr_Questions[numQuest].Reponses[2].description;
                Rep4.text = questions.arr_Questions[numQuest].Reponses[3].description;
                break;
            case "1":
                btnShow.changeObjInter(ReponsesT1);
                repFamily = repFamilyT1;
                Rep11.text = questions.arr_Questions[numQuest].Reponses[0].description;
                Rep21.text = questions.arr_Questions[numQuest].Reponses[1].description;
                Rep31.text = questions.arr_Questions[numQuest].Reponses[2].description;
                Rep41.text = questions.arr_Questions[numQuest].Reponses[3].description;
                break;
            default:
                Debug.Log("ERREUR");
                break;
        }

        countQuest++;
        numQuestText.text = countQuest + "";
    }

    public void nextQuest()
    {
        if (repFamily.selectedToggle() != 4)
        {
            points = points + questions.arr_Questions[listNumQuest[countQuest - 1]].Reponses[repFamily.selectedToggle()].point;
        }
        if (countQuest < 5)
        {
            chargementQuest(listNumQuest[countQuest]);
        }
        else
        {
            popupFin.SetActive(true);
            resultats.memory = points.ToString();
            resultats.memoryPlay = "1";
            resultats.noteGlobal = resultats.noteGlobal + points;
            resultats.noteFinal = resultats.noteGlobal / 4;

            //La sauvegarde dans le fichier correspondant
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
            {
                if (Application.platform == RuntimePlatform.Android)
                {
                    resultats.Save(Application.persistentDataPath + "/Resultats.xml");
                }
                else
                {
                    string xmlconvert = XmlOperation.SerializeObject(resultats);
                    SaveDataSession(xmlconvert);
                    Debug.Log(xmlconvert);
                }
            }
            else
            {
                resultats.Save(Application.dataPath + "/StreamingAssets/XML/Resultats.xml");
            }
            Debug.Log(points);
            Debug.Log("Fin du memory");
        }
    }
}
