﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnectionAdmin : MonoBehaviour
{
    public InputField pass;
    public Text textErr;
    public GameObject messErr;
    public GameObject popupResultat;
    public GameObject popupAdmin;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ConnectionAdminBtn()
    {

        if (pass.text == "")
        {
            textErr.text = "Veuillez saisir un mot de passe !";
            messErr.SetActive(true);
            //Debug.Log("Saisir mdp");
        }
        if (pass.text != "family2020!")
        {
            Debug.Log(pass.text);
            textErr.text = "Mot de passe incorrect !";
            messErr.SetActive(true);
            //Debug.Log("Saisir mdp");
        }
        else
        {
            messErr.SetActive(false);
            popupResultat.SetActive(true);
            popupAdmin.SetActive(false);
            //textErr.text = "Informations incorrectes";
            //messErr.SetActive(true);
        }
        /*else
        {
            SceneManager.LoadScene("NewSessionScene", LoadSceneMode.Single);
        }*/
    }
}
